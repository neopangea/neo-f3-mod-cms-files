# FILES MODULE - NEO F3 CMS MODULE #

This is a file manager to be used w/ [NEO F3 CMS](https://bitbucket.org/neopangea/neo-f3-mod-cms)

### Installation ###

1. Install [application framework](https://bitbucket.org/neopangea/neo-f3-app) 
2. Install [cms framework](https://bitbucket.org/neopangea/neo-f3-mod-cms)
3. Add a [composer dependency](https://packagist.org/packages/neopangea/f3-mod-cms-files) for this project to root project's composer.json
4. Run via command line 'composer update'
5. Done!

### Usage ###

Once installed a Files node will appear in CMS menu system. Click it and work w/ the (hopefully) intuitive interface. 

### Configuration ###

* By default all uploaded files will placed in the root app's /public/uploads/content directory. To change this behavior add NEO_CMS_FILES_UPLOAD_PATH = <new path> to the root app's /config/common.ini file. Note: use web root relative (no leading slash) path.