<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 8/13/14
 * Time: 1:04 PM
 */

namespace Neo\Cms\Files;

use \Neo\Cms\Lib      as Neo;
use \Neo\Lib\Enums    as Enums;
use \Neo\Lib\Utils    as Utils;

class FilesController extends \Neo\F3\Controller {

    public function index( $f3, $args ) {
        parent::__construct( $f3 );

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);

        $this->f3->set('nav.primary'   , 'files');
        $this->f3->set('nav.secondary' , 'files:index');

        $template = \Template::instance();
        echo $template->render('vendor/neopangea/f3-mod-cms-files/templates/file-manager.htm');
    }

    public function getFileManagerViewModel( $f3, $args ) {

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);

        $file_type = isset($_GET['file_type']) ? $_GET['file_type'] : Enums\FileExt::NEO_FILE_TYPE_IMAGE;

        $path = Utils\File::trimRightSlash($f3->get('NEO_CMS_FILES_UPLOAD_PATH')) . "/{$file_type}";
        Utils\File::makeMissingDir($path);

        $response = new \Neo\F3\Response();

        $response->data->files = array();

        $file_names = scandir($path, SCANDIR_SORT_ASCENDING);
        foreach ($file_names as $file_name) {
            if ('.' == Utils\String::getFirstLetter($file_name)) {continue;}
            $file = new \stdClass();
            $file->name = $file_name;
            $file->url = '/' . $path . '/' . $file->name;
            $file->fullUrl = '//' . $f3->get('NEO_APP_DOMAIN') . '/' . $path . '/' . $file->name;
            $file->type = Utils\File::getFileType($file_name);
            $file->updated_at = filemtime($path . '/' . $file->name);
            array_push($response->data->files, $file);
        }

        $response->data->file_type_options = Enums\FileExt::getAllFileTypes();
        $response->data->file_type = $file_type;
        $response->successful = true;

        exit(json_encode($response));
    }

    public function upload( $f3, $args ) {

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);

        $response = new \Neo\F3\Response();

        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($_FILES['uploadfile']['error']) ||
                is_array($_FILES['uploadfile']['error'])
            ) {
                throw new \Exception('Invalid parameters.');
            }

            // Check $_FILES['uploadfile']['error'] value.
            switch ($_FILES['uploadfile']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new \Exception('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new \Exception('Exceeded filesize limit.');
                default:
                    throw new \Exception('Unknown errors.');
            }

            $file_type   = Utils\File::getFileType($_FILES['uploadfile']['name']);
            $upload_path = Utils\File::trimRightSlash($f3->get('NEO_CMS_FILES_UPLOAD_PATH')) . "/{$file_type}";

            $filename = $_FILES['uploadfile']['name'];

            // BEGIN Unique Name
            // TODO Create a global setting for this behavior to be toggled
            $forceUniqueName = false;
            $unique_name = false;

            if($this->checkFileExists($upload_path, $filename)){
                $unique_name = $this->uniqueFileName($upload_path, $filename);
            };

            if($unique_name && $forceUniqueName){
                $filename = $unique_name;
            }
            // END Unique Name

            if (!move_uploaded_file(
                $_FILES['uploadfile']['tmp_name'],
                $upload_path . '/' . $filename
            )) {
                throw new \Exception('Failed to move uploaded file.');
            };

            // Construct view model
            $file = new \stdClass();
            $file->name = $_FILES['uploadfile']['name'];
            $file->url = '/' . $upload_path . '/' . $file->name;
            $file->type = $file_type;
            $file->updated_at = time();

            $response->data->files = array($file);

        } catch (\Exception $e) {

            $response->successful = false;
            $response->message    = $e->getMessage();
        }

        exit(json_encode($response));
    }

    /**
     * Deletes file, throws exception if file is not in config upload path
     * @param $f3
     * @param $args
     */

    public function delete ( $f3, $args ) {

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);

        $response = new \Neo\F3\Response();

        try {

            if (!isset($_GET['url'])) { throw new \Exception('Variable: "url" is missing.'); }

            $url = '/' . Utils\File::trimLeftSlash($_GET['url']);

            if (0 !== strpos($url, '/' . Utils\File::trimLeftSlash($f3->get('NEO_CMS_FILES_UPLOAD_PATH')))) {
                throw new \Exception('Illegal directory');
            }

            unlink(Utils\File::trimLeftSlash($url));

            $response->data->url = $url;

        } catch (\Exception $e) {

            $response->successful = false;
            $response->message    = $e->getMessage();
        }

        exit(json_encode($response));
    }

    public function checkFileExists($upload_path, $filename){

        $exists = false;

        $existing_files = scandir($upload_path, SCANDIR_SORT_ASCENDING);

        foreach ($existing_files as $existing_file) {
            if ('.' == Utils\String::getFirstLetter($existing_file)) {
                continue;
            }

            if($existing_file == $filename){
                $exists = true;
            }
        }

        return $exists;
    }

    public function uniqueFileName($upload_path, $filename){

        $fileParts = explode(".", $filename);

        $random = $this->generateRandomString();

        $name = $fileParts[0] .'-'.$random;
        $extension = $fileParts[1];

        $new_filename = $name .'.'. $extension;

        if($this->checkFileExists($upload_path, $new_filename)){
            $new_filename = $this->uniqueFileName($upload_path, $new_filename);
        };

        return $new_filename;

    }

    public function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}