<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/4/14
 * Time: 1:13 PM
 */

namespace Neo\Cms\Files;

use \Neo\Cms\Lib   as Neo;
use \Neo\Lib\Enums as Enums;
use \Neo\Lib\Utils as Utils;

class SyncController extends \Neo\F3\Controller {

    public function index( $f3, $args ) {
        parent::__construct( $f3 );

        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);

        $this->f3->set('nav.primary'   , 'files');
        $this->f3->set('nav.secondary' , 'files:sync:index');

        $template = \Template::instance();
        echo $template->render('vendor/neopangea/f3-mod-cms-files/templates/file-sync.htm');
    }

    public function sync( $f3, $args ) {
        parent::__construct( $f3 );

        $response = new \Neo\F3\Response();

        try {
            $action = $_GET['action'];
            switch ($action) {
                case 'get-external-file-list':
                    if(!isset($_GET['syncToUrl'])) { throw new \Exception('syncToUrl variable missing.'); }

                    // Local list
                    $localFiles = static::getLocalFiles($f3);

                    //External List
                    $syncToUrl = Utils\File::trimRightSlash($_GET['syncToUrl']);

                    // Read file contents served up by api
                    $curl = curl_init($syncToUrl . '/cms/services/files/sync?action=get-local-file-list');
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    $curlResponse = curl_exec($curl);
                    curl_close($curl);
                    if ($curlResponse === false) {
                        throw new \Exception('Error in getting response from external server.');
                    }

                    $externalData = json_decode( $curlResponse );
                    $externalFiles = $externalData->data->localFiles;

                    $files = array();
                    foreach ($externalFiles as $externalFile) {
                        $externalFile->isLocal = in_array($externalFile, $localFiles);
                        $files[] = $externalFile;
                    }
                    $response->data->files = $files;
                    break;
                case 'get-local-file-list':
                    $response->data->localFiles = static::getLocalFiles($f3);
                    break;
                case 'sync-file':
                    if(!isset($_GET['syncToUrl']))  { throw new \Exception('syncToUrl variable missing.' ); }
                    if(!isset($_GET['syncToPath'])) { throw new \Exception('syncToPath variable missing.'); }
                    $syncToUrl = Utils\File::trimRightSlash($_GET['syncToUrl']) . '/' . Utils\File::trimLeftSlash($_GET['syncToPath']);
                    $localUrl  = Utils\File::trimLeftSlash($_GET['syncToPath']);
                    Utils\File::makeMissingDir($localUrl);

                    // Copy the file over
                    $fp = fopen ($localUrl, 'w+'); //This is the file where we save the    information
                    $ch = curl_init(str_replace(" ", "%20", $syncToUrl)); //Here is the file we are downloading, replace spaces with %20
                    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
                    curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_exec($ch); // get curl response
                    curl_close($ch);
                    fclose($fp);

                    break;
                default:
                    throw new \Exception('Unknown action: ' . $action);
            }
        } catch (\Exception $e) {
            $response->successful = false;
            $response->message = $e->getMessage();
        }

        exit(json_encode($response));
    }

    private static function getLocalFiles ($f3) {

        $root = Utils\File::trimRightSlash($f3->get('NEO_CMS_FILES_UPLOAD_PATH'));
        Utils\File::makeMissingDir($root);

        $iter = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($root, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST,
            \RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
        );

        $files = array();
        foreach ($iter as $path => $item) {
            if (!$item->isDir()) {
                $file = new \stdClass();
                $file->url  = '/'.$path;

                $file->url = str_replace('\\', '/', $file->url);

                $file->type = Utils\File::getFileType($item);
                $files[] = $file;
            }
        }

        return $files;
    }
}