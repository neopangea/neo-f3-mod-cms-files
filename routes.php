<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/20/14
 * Time: 6:39 PM
 */

$app = Neo\F3\App::instance();

// File Manager
$app->f3->route( 'GET    /'.  $app->f3->get('NEO_CMS_SLUG') .'/files'                             , 'Neo\Cms\Files\FilesController->index'                   );
$app->f3->route( 'POST   /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/files/upload'             , 'Neo\Cms\Files\FilesController->upload'                  );
$app->f3->route( 'GET    /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/view-models/file-manager' , 'Neo\Cms\Files\FilesController->getFileManagerViewModel' );
$app->f3->route( 'DELETE /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/view-models/file-manager' , 'Neo\Cms\Files\FilesController->delete' );
// File Sync
$app->f3->route( 'GET   /'.  $app->f3->get('NEO_CMS_SLUG') .'/files/sync'            , 'Neo\Cms\Files\SyncController->index' );
$app->f3->route( 'GET   /'.  $app->f3->get('NEO_CMS_SLUG') .'/services/files/sync'   , 'Neo\Cms\Files\SyncController->sync'  );

