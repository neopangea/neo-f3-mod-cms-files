/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global NP, angular, window, _*/

(function () {

    'use strict';

    angular.module('fileSyncApp').controller(
        'fileSyncController',
        [
            '$scope',
            '$q',
            'fileSyncFactory',
            function ($scope, $q, fileSyncFactory) {

                $scope.data  = {
                    syncToUrl : '',
                    syncFileOptions : []
                };
                $scope.ui = {
                    state : {
                        current : 'default',
                        set : function (state) {
                            $scope.ui.state.current = state;
                        }
                    }
                };

                /**
                 * Go get a fresh copy of file sync options
                 */

                $scope.refreshFileOptions = function () {

                    $scope.error = '';
                    $scope.ui.state.set('refreshing');

                    fileSyncFactory.get({
                            'action'    : 'get-external-file-list',
                            'syncToUrl' : $scope.data.syncToUrl
                        },
                        function (response) {

                            if (response.successful !== true) {
                                window.console.warn('error', response);
                                $scope.error = response.message;
                                $scope.ui.state.set('default');
                                return;
                            }

                            $scope.data.syncFileOptions = response.data.files;

                            $scope.ui.state.set('refreshed');
                        },
                        function (data) {
                            window.console.warn('error', data);
                            $scope.error = 'Server error. Check JS console for details.';
                            $scope.ui.state.set('default');
                        }
                    );
                };

                /**
                 * Toggle the checked fileOption
                 * @param fileOption
                 */

                $scope.toggleChecked = function (fileOption) {
                    if (!fileOption.checked) {
                        fileOption.checked = false;
                        return;
                    }
                    fileOption.checked = true;
                };

                /**
                 * Quick select file handler
                 * @param method
                 */

                $scope.quickSelect = function(method) {
                    switch (method) {
                        case 'none':
                            _.each($scope.data.syncFileOptions, function (item) {
                                item.checked = false;
                            }, this);
                            break;
                        case 'all':
                            _.each($scope.data.syncFileOptions, function (item) {
                                item.checked = true;
                            }, this);
                            break;
                        case 'missing':
                            _.each($scope.data.syncFileOptions, function (item) {
                                item.checked = !item.isLocal;
                            }, this);
                            break;
                    }
                };

                /**
                 * Recursively called file sync, call initially with i undefined
                 * @param i
                 */
                $scope.syncFiles = function (i) {

                    /**
                     * Set i to 0 first around
                     */

                    i = i || 0;

                    window.console.log('syncFiles: ' , i);

                    /**
                     * Return when if all file options processed, and set state to 'synced'
                     */

                    if(i >= $scope.data.syncFileOptions.length) {
                        $scope.ui.state.set('synced');
                        return;
                    }

                    $scope.ui.state.set('syncing');

                    /**
                     * Get the model and tick the counter
                     */

                    var syncFile = $scope.data.syncFileOptions[i];
                    i = i + 1;

                    /**
                     * Return if user did not check this file
                     */

                    if (!syncFile.checked) {
                        $scope.syncFiles(i);
                        return;
                    }

                    syncFile.status = 'syncing';

                    fileSyncFactory.get({
                            'action'     : 'sync-file',
                            'syncToUrl'  : $scope.data.syncToUrl,
                            'syncToPath' : syncFile.url
                        },
                        function (response) {
                            $scope.syncFiles(i);
                            if (response.successful !== true) {
                                syncFile.status  = 'error';
                                window.console.warn('error', response);
                                $scope.ui.state.set('error');
                                return;
                            }
                            syncFile.isLocal = true;
                            syncFile.status  = 'synced';

                        }, function (data) {
                            window.console.log('error response...', data);
                            $scope.ui.state.set('error');
                        }
                    );

                };
            }
        ]
    );
}());

