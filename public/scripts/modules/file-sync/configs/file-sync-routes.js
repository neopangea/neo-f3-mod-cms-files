/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('fileSyncApp').config([
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/', {
                    controller: 'fileSyncController',
                    templateUrl: '/vendor/neopangea/f3-mod-cms-files/public/scripts/modules/file-sync/views/file-sync-view.html'
                }).otherwise({
                    redirectTo: '/'
                });
        }
    ]);

}());