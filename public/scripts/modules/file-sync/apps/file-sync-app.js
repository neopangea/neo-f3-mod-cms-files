/*global angular*/

(function () {
    'use strict';

    angular.module('fileSyncApp', [
        'neoCmsBaseModule',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ]);

}());
