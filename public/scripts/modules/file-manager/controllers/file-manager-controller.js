/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global NP, angular, window, Blazy, _*/

(function () {

    'use strict';

    angular.module('filesApp').controller(
        'fileManagerController',
        [
            '$scope',
            '$rootScope',
            'filesViewModelFactory',
            function ($scope, $rootScope, filesViewModelFactory) {

                $scope.ui    = {};
                $scope.data  = {};
                $scope.uploadErrors = [];
                $scope.config = NP.config;
                $scope.files = {
                    current  : [],
                    uploaded : [],
                    cache    : {}
                };

                $scope.state = {
                    options: ['default', 'fetching', 'error'],
                    current: 'default',
                    msg: '',
                    set: function (state, msg) {
                        if ($scope.state.options.indexOf(state) === -1) {
                            window.console.warn('unknown state: ', state);
                        }
                        $scope.state.msg = msg || '';
                        $scope.state.current = state;
                    }
                };

                /**
                 * Call this to get files from the server. File type (see Neo\Lib\Enums\FileExt) must be given.
                 * Files are segmented by file type to reduce file GET overhead.
                 * @param file_type
                 */

                $scope.getFiles = function (file_type) {

                    if ($scope.files.cache[file_type]) {
                        $scope.files.current = $scope.files.cache[file_type];
                        return;
                    }

                    $scope.state.set('fetching');

                    filesViewModelFactory.get(
                        {file_type:$scope.ui.file_type_filter.current},
                        function (response) {
                            $scope.state.set('default');
                            $scope.data.file_type_options = response.data.file_type_options;
                            $scope.data.file_type         = response.data.file_type_options;
                            $scope.addFiles(response.data.files);
                            $scope.files.current = $scope.files.cache[file_type];
                            $scope.lazyLoader.refresh();
                        }
                    );
                };

                /**
                 * Add files to scopes file cache.
                 * @param files
                 */

                $scope.addFiles = function (files, options) {
                    options = _.extend({
                        uploaded : false
                    }, options);

                    _.each(files, function (file) {
                        if (!$scope.files.cache[file.type]) {
                            $scope.files.cache[file.type] = [];
                        }
                        $scope.files.cache[file.type].push(file);

                        if (options.uploaded) {
                            $scope.files.uploaded.push(file);
                        }
                    });
                    $scope.lazyLoader.refresh();
                };

                /**
                 * Fires 'file-chosen' event for choosing file
                 * @param url
                 */

                $scope.chooseFile = function (url) {
                    $scope.$emit('file-chosen', url);
                };

                /**
                 * Delete file from server
                 * @param file
                 */

                $scope.deleteFile = function (file) {

                    window.console.log('Deleting file: ', file.url);

                    filesViewModelFactory.remove(
                        {url:file.url},
                        function (response) {
                            if (response.successful) {
                                _.find(
                                    $scope.files.current, function(elem, index){
                                        console.log('checking: ', elem.url);

                                        if (file.url === elem.url) {
                                            console.log('*** Removing: ', $scope.files.current[index].url);
                                            $scope.files.current.splice(index, 1);
                                            //$scope.lazyLoader.refresh();
                                            return true;
                                        }
                                        return false;
                                }, this);
                            }
                        }
                    );
                };

                /**
                 * Helper to manage table filter.
                 * @type {{current: string, set: set}}
                 */

                $scope.ui.file_type_filter = {
                    current: 'image',
                    set: function (file_type) {
                        this.current = file_type;

                        switch ($scope.ui.panel.current) {
                            case 'file-list':
                            case 'file-thumbs':
                                $scope.getFiles($scope.ui.file_type_filter.current);
                                break;
                            case 'file-uploader':
                                break;
                        }
                        $scope.lazyLoader.refresh();
                    }
                };

                /**
                 * Helper to manage which panels are visible
                 * @type {{current: string, set: set}}
                 */

                $scope.ui.panel = {
                    current : 'file-list',
                    set : function (panel) {
                        this.current = panel;

                        switch ($scope.ui.panel.current) {
                            case 'file-list':
                            case 'file-thumbs':
                                break;
                            case 'file-uploader':
                                $scope.uploadErrors = [];
                                break;
                        }
                        $scope.lazyLoader.refresh();
                    }
                };

                /**
                 * Helper to manage table sorts.
                 * @type {{field: string, descending: boolean, headers: *[], set: set}}
                 */

                $scope.ui.sort = {
                    field: 'updated',
                    isDescending: true,
                    headers: [
                        {label : 'Type'      , field : 'type'      },
                        {label : 'Updated'   , field : 'updated'},
                        {label : 'File Name' , field : 'name'      }],
                    set: function (field) {
                        if (this.field === field) {
                            this.isDescending = !this.isDescending;
                        } else {
                            this.field = field;
                            this.isDescending = true;
                        }
                        $scope.lazyLoader.refresh();
                    }
                };

                /**
                 * Lazy Loading
                 */

                $scope.lazyLoader = {
                    blazy   : new Blazy({src: 'data-lazy-src'}),
                    refresh : function () {
                        if (this.isThrottled) { return; }
                        this.isThrottled = true;
                        window.setTimeout(
                            _.bind(function () {
                                this.isThrottled = false;
                                this.blazy.revalidate();
                            }, this),
                            this.throttleDuration
                        );
                        this.blazy.revalidate();
                    },
                    isThrottled : false,
                    throttleDuration : 500
                };

                $scope.$on('lazy-load-refresh', function () {
                    $scope.lazyLoader.refresh();
                });

                /**
                 * Initialize
                 */
                // Use w/ ng-init="doNotInitFileManager = true;" if there are many instances on one page, the get
                // files opperation is very expensive
                $scope.$on('file-manager-refresh-files', function () {
                    $scope.getFiles($scope.ui.file_type_filter.current);
                });

                if($scope.doNotInitFileManager) {
                    window.console.log('*** file manager set not to init.');
                } else {
                    $scope.getFiles($scope.ui.file_type_filter.current);
                }
            }
        ]
    );
}());

