/*global angular*/

(function () {
    'use strict';

    angular.module('filesApp', [
        'neoCmsBaseModule',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ]);

}());
