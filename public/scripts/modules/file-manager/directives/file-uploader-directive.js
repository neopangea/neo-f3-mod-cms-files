/*jslint todo: true, nomen: true, white: true */
/*global angular, _, console, alert, $*/

(function () {
    'use strict';

    angular.module('filesApp').directive('neoFileUploader', [
        'config',
        function (config) {
            return {
                link : function (scope, element) {

                    var dropzoneItemTemplate = $('dropzone-item-template').html();

                    element.dropzone({
                        dictDefaultMessage: 'Click Here to Upload Files',
                        url: "/"+ config.cmsSlug +"/services/files/upload",
                        maxFilesize: 100,
                        paramName: "uploadfile",
                        previewsContainer: "#previews",
                        maxThumbnailFilesize: 5,
                        init: function () {
                            this.on('success', function (file, response) {
                                response = JSON.parse(response);

                                if (!response.successful) {
                                    scope.$apply(function () {
                                        scope.uploadErrors.push({
                                            fileName:file.name,
                                            msg: response.message
                                        });
                                    });
                                }

                                scope.$apply(function () {
                                    scope.addFiles(
                                        response.data.files,
                                        {uploaded : true}
                                    );
                                });
                            });
                            this.on('addedfile', function (file) {
                            });
                            this.on('error', function (file, errorMsg) {
                                console.log('Error uploading file', file );
                                scope.$apply(function () {
                                    scope.uploadErrors.push({
                                        fileName:file.name,
                                        msg: errorMsg
                                    });
                                });
                            });
                        }
                    });
                }
            }
        }
    ]);
}());