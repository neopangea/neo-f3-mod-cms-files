/*jslint todo: true, nomen: true, white: true */
/*global angular, _, console, window, $, Blazy*/

(function () {
    'use strict';

    angular.module('filesApp').directive('neoFileManager', [
        function () {
            return {
                scope: {},
                require: "ngModel",
                link : function (scope, element, attrs, ngModel) {

                    /**
                     * If input is updated manually (copy & paste) update the model and preview url value
                     */
                    element.find('.file-manager-input').change(function (evt) {
                        var url = $(evt.target)[0].value;
                        ngModel.$setViewValue(url);
                        element.find('.file-manager-preview').attr('href', url);
                    });

                    /**
                     * Update the url in the view elements
                     * @param url
                     */
                    scope.setViewUrls = function (url) {
                        element.find('.file-manager-input').val(url);
                        element.find('.file-manager-preview').attr('href', url);
                    };

                    /**
                     * Initialize input with model value
                     */

                    scope.$watch(function () {
                        return ngModel.$modelValue;
                    }, function(newValue) {
                        scope.setViewUrls(newValue);
                    });

                    /**
                     * Listen for 'file-chosen' event emitted from filemanager and update model
                     */

                    scope.$on('file-chosen', function (evt, url) {
                        evt.stopPropagation();
                        // Set model value and update form input display
                        ngModel.$setViewValue(url);
                        scope.setViewUrls(ngModel.$viewValue);
                    });

                    /**
                     * Init lazy loader
                     *
                     * TODO: This is duplicate code. See file-manager-controller for original. Did this b/c could not
                     * figure out clean communication between modal and main controller
                     *
                     */

                    scope.lazyLoader = {
                        blazy   : new Blazy({src: 'data-lazy-src'}),
                        refresh : function () {
                            if (this.isThrottled) { return; }
                            this.isThrottled = true;
                            window.setTimeout(
                                _.bind(function () {
                                    this.isThrottled = false;
                                    this.blazy.revalidate();
                                }, this),
                                this.throttleDuration
                            );
                            this.blazy.revalidate();
                        },
                        isThrottled : false,
                        throttleDuration : 500
                    };

                    /**
                     * Scroll refreshes lazy loader
                     */

                    element.find('.modal-content').on('scroll', function(){
                        scope.lazyLoader.refresh();
                    });

                    /**
                     * Open modal
                     */

                    element.find('.file-manager-open-modal').on('click', function () {

                        scope.$broadcast('file-manager-refresh-files');

                        element.find('.file-manager-modal').modal();
                        scope.lazyLoader.refresh();
                    });
                },

                templateUrl: function(elem,attrs) {
                    return attrs.templateUrl || '/vendor/neopangea/f3-mod-cms-files/public/scripts/modules/file-manager/partials/file-manager-directive-partial.html';
                }

            };
        }
    ]);
}());