/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('filesApp').config([
        '$routeProvider',
        function ($routeProvider) {

            $routeProvider
                .when(
                '/', {
                    redirectTo: '/list'
                })
                .when('/list', {
                    controller: 'fileManagerController',
                    templateUrl: '/vendor/neopangea/f3-mod-cms-files/public/scripts/modules/file-manager/views/file-manager-view.html'
                })
        }

    ]);

}());